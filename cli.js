#!/usr/bin/env node

const { exec } = require('child_process');
const arguments = process.argv.slice(2);

let build_type = 'default';
switch (arguments[0]) {
  case 'theme':
    build_type = 'theme';
    console.log('Building theme');
    break;

  default:
    console.log('Building default');
}

let build_mode = 'production';
switch (arguments[1]) {
  case 'development':
    build_mode = 'development';
    console.log('Building in development mode');
    break;

  default:
    console.log('Building in production mode');
}

exec(`webpack --config ./node_modules/@testbysteven/default-webpack/config/${build_type}.js --mode ${build_mode}`, (err, stdout, stderr) => {
  if (err) {
    console.log(err)
    return;
  }

  if (stdout) {
    console.log(stdout);
  }

  if (stderr) {
    console.log(stderr);
  }
});
