## Install

Add gitlab registry to the `.npmrc`:
```
echo '@testbysteven:registry=https://gitlab.com/api/v4/packages/npm' >> .npmrc
```

After npm install you have to install peer dependencies:
You can use https://www.npmjs.com/package/install-peerdeps with:
`install-peerdeps @testbysteven/default-webpack -r https://gitlab.com/api/v4/packages/npm`

Add scripts to your package.json:
```
"scripts": {
  "build": "./node_modules/.bin/default-webpack"
},
```

## Arguments
type = default
mode = production

`default-webpack theme development` outputs theme.js in development mode.
