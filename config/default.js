const path = require('path');
const webpack = require('webpack');

const project_dir = path.join(__dirname, '..', '..', '..', '..')

const config = {
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },

  entry: {
    customer: ['./src/scripts/main.js']
  },

  output: {
    chunkFilename: 'scripts/[name].js',
    filename: 'scripts/[name].js',
    path: path.join(project_dir, 'static')
  }
}

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map';
  }

  return config;
};
